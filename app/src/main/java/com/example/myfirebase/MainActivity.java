package com.example.myfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myfirebase.Model.Karyawan;
import com.example.myfirebase.ui.AdapterKaryawan;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterKaryawan.FirebaseDataListener {
    AdapterKaryawan adapterKaryawan;

    private EditText inputNama, inputDivisi, inputGaji;
    private String valueNama, valueDivisi, valueGaji;
    private Button btnRegister;

    private RecyclerView recyclerView;

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private List<Karyawan> karyawanList = new ArrayList<>();

    Context context;
    private DatabaseReference databaseReference;
    private ArrayList<Karyawan> registerKaryawan;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        FirebaseApp.initializeApp(this);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("karyawan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                registerKaryawan = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()){
                    Karyawan karyawan = noteDataSnapshoot.getValue(Karyawan.class);
                    karyawan.setKey(noteDataSnapshoot.getKey());
                    registerKaryawan.add(karyawan);
                }
                adapter = new AdapterKaryawan(MainActivity.this, registerKaryawan);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });

        final Karyawan karyawan = (Karyawan) getIntent().getSerializableExtra("data");
        if (karyawan != null){
            inputNama.setText(karyawan.getNama());
            inputDivisi.setText(karyawan.getDivisi());
            inputGaji.setText(String.valueOf(karyawan.getGaji()));

            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    karyawan.setNama(inputNama.getText().toString());
                    karyawan.setDivisi(inputDivisi.getText().toString());
                    karyawan.setGaji(Integer.parseInt(String.valueOf(inputGaji.getText())));

                    updateKaryawan(karyawan);
                }
            });

        }else {
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    validation();
                }
            });
        }


    }

    private void setupView() {
        recyclerView = findViewById(R.id.recyclerview);
        inputNama = findViewById(R.id.editValueNama);
        inputDivisi = findViewById(R.id.editValueDivisi);
        inputGaji = findViewById(R.id.editValueGaji);
        btnRegister = findViewById(R.id.buttonDaftar);
    }

    private void validation() {
        valueNama = inputNama.getText().toString();
        valueDivisi = inputDivisi.getText().toString();
        valueGaji = inputGaji.getText().toString();

        boolean isEmptyText = false;

        if (TextUtils.isEmpty(valueNama)){
            isEmptyText = true;
            inputNama.setError("Please fill this field");
        }
        if (TextUtils.isEmpty(valueDivisi)){
            isEmptyText = true;
            inputDivisi.setError("Please fill this field");
        }
        if (TextUtils.isEmpty(valueDivisi)){
            isEmptyText = true;
            inputDivisi.setError("Please fill this field");
        }
        if (!isEmptyText){
            registerinKaryawan();
        }
    }

    private boolean isEmpty(String s){
        return TextUtils.isEmpty(s);
    }

    private void registerinKaryawan() {
        if (!isEmpty(inputNama.getText().toString())
                && !isEmpty(inputDivisi.getText().toString())
                && !isEmpty(inputGaji.getText().toString())){
            submitKaryawan(new Karyawan(
                    inputNama.getText().toString(),
                    inputDivisi.getText().toString(),
                    Integer.parseInt(String.valueOf(inputGaji.getText()))
            ));

            Toast.makeText(getBaseContext(), "Registered", Toast.LENGTH_SHORT).show();

            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(inputNama.getWindowToken(), 0);
        }
    }

    private void submitKaryawan(Karyawan karyawan) {
        databaseReference.child("karyawan")
                .push()
                .setValue(karyawan)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        inputNama.setText("");
                        inputDivisi.setText("");
                        inputGaji.setText("");
                        Snackbar.make(findViewById(R.id.buttonDaftar),"Success registered",Snackbar.LENGTH_LONG);
                    }
                });
    }

    @Override
    public void onDelete(Karyawan karyawan, int position) {
        if (databaseReference != null){
            databaseReference.child("karyawan").child(karyawan.getKey())
                    .removeValue()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(MainActivity.this, "Delete", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void updateKaryawan(Karyawan karyawan) {
        databaseReference.child("karyawan")
                .child(karyawan.getKey())
                .setValue(karyawan)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.buttonDaftar),
                                "Update success",
                                Snackbar.LENGTH_LONG)
                                .setAction("See", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        finish();
                                    }
                                }).show();
                    }
                });
    }

    public static Intent getActIntent(Activity context) {

        return new Intent(context, MainActivity.class);
    }





}
