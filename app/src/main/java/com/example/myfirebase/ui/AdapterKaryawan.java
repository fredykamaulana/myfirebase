package com.example.myfirebase.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfirebase.MainActivity;
import com.example.myfirebase.Model.Karyawan;
import com.example.myfirebase.R;

import java.util.ArrayList;

public class AdapterKaryawan extends RecyclerView.Adapter<AdapterKaryawan.MyViewHolder> {
    
    private Context context;
    private ArrayList<Karyawan> karyawanList;

    FirebaseDataListener listener;

    public AdapterKaryawan(Context context, ArrayList<Karyawan> karyawanArrayList) {
        this.context = context;
        this.karyawanList = karyawanArrayList;
        this.listener = (MainActivity) context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_karyawan,
                        parent,
                        false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final Karyawan karyawan = karyawanList.get(position);

        holder.setModel(karyawan);

        holder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(R.layout.layout_dialog);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                Button btnEdit = alertDialog.findViewById(R.id.btnEdit);
                Button btnDelete = alertDialog.findViewById(R.id.btnDelete);

                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        context.startActivity(MainActivity.
                                getActIntent((Activity)context).
                                putExtra("data",
                                        karyawanList.get(position)));
                    }
                });

                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        listener.onDelete(karyawanList.get(position),position);
                    }
                });

                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        if (karyawanList != null){
            return karyawanList.size();
        }else {
            return 0;
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Karyawan karyawanList;
        TextView valueNama, valueDivisi, valueGaji;
        LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            valueNama = itemView.findViewById(R.id.textValueNama);
            valueDivisi = itemView.findViewById(R.id.textValueDivisi);
            valueGaji = itemView.findViewById(R.id.textValueGaji);
            linearLayout = itemView.findViewById(R.id.layoutKaryawan);
        }

        public void setModel(Karyawan karyawan) {
            karyawanList = karyawan;

            valueNama.setText(karyawan.getNama());
            valueDivisi.setText(karyawan.getDivisi());
            valueGaji.setText("Rp. " + karyawan.getGaji());
        }
    }

    public interface FirebaseDataListener {
        void onDelete(Karyawan karyawan, int position);
    }
}
