package com.example.myfirebase.Model;

import java.io.Serializable;

public class Karyawan implements Serializable {

    private String nama;
    private String divisi;
    private int gaji;
    private String key;

    public Karyawan(){}

    public Karyawan(String nama, String divisi, int gaji) {
        this.nama = nama;
        this.divisi = divisi;
        this.gaji = gaji;
        this.key = key;
    }

    public String toString(){
        return  " "+nama+"\n"+
                " "+divisi+"\n"+
                " "+gaji;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
